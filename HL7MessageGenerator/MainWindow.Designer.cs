﻿namespace HL7MessageGenerator
{
    partial class MainWindow
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        public void InitializeComponent()
        {
            this.txtBoxPersonData = new System.Windows.Forms.TextBox();
            this.btnPersonData = new System.Windows.Forms.Button();
            this.labelPersonData = new System.Windows.Forms.Label();
            this.openPersonData = new System.Windows.Forms.OpenFileDialog();
            this.labelExaminer = new System.Windows.Forms.Label();
            this.btnExaminer = new System.Windows.Forms.Button();
            this.txtBoxExaminer = new System.Windows.Forms.TextBox();
            this.labelRemarks = new System.Windows.Forms.Label();
            this.btnRemarks = new System.Windows.Forms.Button();
            this.txtBoxRemarks = new System.Windows.Forms.TextBox();
            this.labelIssues = new System.Windows.Forms.Label();
            this.btnIssues = new System.Windows.Forms.Button();
            this.txtBoxIssues = new System.Windows.Forms.TextBox();
            this.labelRefferingPhysician = new System.Windows.Forms.Label();
            this.btnRefferingPhysician = new System.Windows.Forms.Button();
            this.txtBoxRefferingPhysician = new System.Windows.Forms.TextBox();
            this.linkDoc = new System.Windows.Forms.LinkLabel();
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtBoxPersonData
            // 
            this.txtBoxPersonData.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.txtBoxPersonData.Location = new System.Drawing.Point(138, 26);
            this.txtBoxPersonData.Name = "txtBoxPersonData";
            this.txtBoxPersonData.ReadOnly = true;
            this.txtBoxPersonData.Size = new System.Drawing.Size(274, 20);
            this.txtBoxPersonData.TabIndex = 0;
            this.txtBoxPersonData.TextChanged += new System.EventHandler(this.txtBoxPersonData_TextChanged);
            // 
            // btnPersonData
            // 
            this.btnPersonData.Location = new System.Drawing.Point(438, 20);
            this.btnPersonData.Name = "btnPersonData";
            this.btnPersonData.Size = new System.Drawing.Size(100, 30);
            this.btnPersonData.TabIndex = 1;
            this.btnPersonData.Text = "Datei öffnen";
            this.btnPersonData.UseVisualStyleBackColor = true;
            this.btnPersonData.Click += new System.EventHandler(this.btnPersonData_Click);
            // 
            // labelPersonData
            // 
            this.labelPersonData.AutoSize = true;
            this.labelPersonData.Font = new System.Drawing.Font("Bahnschrift", 11F);
            this.labelPersonData.Location = new System.Drawing.Point(9, 27);
            this.labelPersonData.Name = "labelPersonData";
            this.labelPersonData.Size = new System.Drawing.Size(110, 18);
            this.labelPersonData.TabIndex = 3;
            this.labelPersonData.Text = "Personendaten:";
            this.labelPersonData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openPersonData
            // 
            this.openPersonData.FileName = "pathPersonData";
            // 
            // labelExaminer
            // 
            this.labelExaminer.AutoSize = true;
            this.labelExaminer.Font = new System.Drawing.Font("Bahnschrift", 11F);
            this.labelExaminer.Location = new System.Drawing.Point(9, 67);
            this.labelExaminer.Name = "labelExaminer";
            this.labelExaminer.Size = new System.Drawing.Size(92, 18);
            this.labelExaminer.TabIndex = 6;
            this.labelExaminer.Text = "Untersucher:";
            this.labelExaminer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExaminer
            // 
            this.btnExaminer.Location = new System.Drawing.Point(438, 60);
            this.btnExaminer.Name = "btnExaminer";
            this.btnExaminer.Size = new System.Drawing.Size(100, 30);
            this.btnExaminer.TabIndex = 5;
            this.btnExaminer.Text = "Datei öffnen";
            this.btnExaminer.UseVisualStyleBackColor = true;
            this.btnExaminer.Click += new System.EventHandler(this.btnExaminer_Click);
            // 
            // txtBoxExaminer
            // 
            this.txtBoxExaminer.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.txtBoxExaminer.Location = new System.Drawing.Point(138, 66);
            this.txtBoxExaminer.Name = "txtBoxExaminer";
            this.txtBoxExaminer.ReadOnly = true;
            this.txtBoxExaminer.Size = new System.Drawing.Size(274, 20);
            this.txtBoxExaminer.TabIndex = 4;
            this.txtBoxExaminer.TextChanged += new System.EventHandler(this.txtBoxExaminer_TextChanged);
            // 
            // labelRemarks
            // 
            this.labelRemarks.AutoSize = true;
            this.labelRemarks.Font = new System.Drawing.Font("Bahnschrift", 11F);
            this.labelRemarks.Location = new System.Drawing.Point(11, 187);
            this.labelRemarks.Name = "labelRemarks";
            this.labelRemarks.Size = new System.Drawing.Size(104, 18);
            this.labelRemarks.TabIndex = 9;
            this.labelRemarks.Text = "Anmerkungen:";
            this.labelRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemarks
            // 
            this.btnRemarks.Location = new System.Drawing.Point(438, 180);
            this.btnRemarks.Name = "btnRemarks";
            this.btnRemarks.Size = new System.Drawing.Size(100, 30);
            this.btnRemarks.TabIndex = 8;
            this.btnRemarks.Text = "Datei öffnen";
            this.btnRemarks.UseVisualStyleBackColor = true;
            this.btnRemarks.Click += new System.EventHandler(this.btnRemarks_Click);
            // 
            // txtBoxRemarks
            // 
            this.txtBoxRemarks.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.txtBoxRemarks.Location = new System.Drawing.Point(138, 186);
            this.txtBoxRemarks.Name = "txtBoxRemarks";
            this.txtBoxRemarks.ReadOnly = true;
            this.txtBoxRemarks.Size = new System.Drawing.Size(274, 20);
            this.txtBoxRemarks.TabIndex = 7;
            this.txtBoxRemarks.TextChanged += new System.EventHandler(this.txtBoxRemarks_TextChanged);
            // 
            // labelIssues
            // 
            this.labelIssues.AutoSize = true;
            this.labelIssues.Font = new System.Drawing.Font("Bahnschrift", 11F);
            this.labelIssues.Location = new System.Drawing.Point(9, 147);
            this.labelIssues.Name = "labelIssues";
            this.labelIssues.Size = new System.Drawing.Size(118, 18);
            this.labelIssues.TabIndex = 12;
            this.labelIssues.Text = "Fragestellungen:";
            this.labelIssues.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIssues
            // 
            this.btnIssues.Location = new System.Drawing.Point(438, 140);
            this.btnIssues.Name = "btnIssues";
            this.btnIssues.Size = new System.Drawing.Size(100, 30);
            this.btnIssues.TabIndex = 11;
            this.btnIssues.Text = "Datei öffnen";
            this.btnIssues.UseVisualStyleBackColor = true;
            this.btnIssues.Click += new System.EventHandler(this.btnIssues_Click);
            // 
            // txtBoxIssues
            // 
            this.txtBoxIssues.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.txtBoxIssues.Location = new System.Drawing.Point(138, 146);
            this.txtBoxIssues.Name = "txtBoxIssues";
            this.txtBoxIssues.ReadOnly = true;
            this.txtBoxIssues.Size = new System.Drawing.Size(274, 20);
            this.txtBoxIssues.TabIndex = 10;
            this.txtBoxIssues.TextChanged += new System.EventHandler(this.txtBoxIssues_TextChanged);
            // 
            // labelRefferingPhysician
            // 
            this.labelRefferingPhysician.AutoSize = true;
            this.labelRefferingPhysician.Font = new System.Drawing.Font("Bahnschrift", 11F);
            this.labelRefferingPhysician.Location = new System.Drawing.Point(9, 107);
            this.labelRefferingPhysician.Name = "labelRefferingPhysician";
            this.labelRefferingPhysician.Size = new System.Drawing.Size(88, 18);
            this.labelRefferingPhysician.TabIndex = 15;
            this.labelRefferingPhysician.Text = "Überweiser:";
            this.labelRefferingPhysician.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRefferingPhysician
            // 
            this.btnRefferingPhysician.Location = new System.Drawing.Point(438, 100);
            this.btnRefferingPhysician.Name = "btnRefferingPhysician";
            this.btnRefferingPhysician.Size = new System.Drawing.Size(100, 30);
            this.btnRefferingPhysician.TabIndex = 14;
            this.btnRefferingPhysician.Text = "Datei öffnen";
            this.btnRefferingPhysician.UseVisualStyleBackColor = true;
            this.btnRefferingPhysician.Click += new System.EventHandler(this.btnRefferingPhysician_Click);
            // 
            // txtBoxRefferingPhysician
            // 
            this.txtBoxRefferingPhysician.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.txtBoxRefferingPhysician.Location = new System.Drawing.Point(138, 106);
            this.txtBoxRefferingPhysician.Name = "txtBoxRefferingPhysician";
            this.txtBoxRefferingPhysician.ReadOnly = true;
            this.txtBoxRefferingPhysician.Size = new System.Drawing.Size(274, 20);
            this.txtBoxRefferingPhysician.TabIndex = 13;
            this.txtBoxRefferingPhysician.TextChanged += new System.EventHandler(this.txtBoxRefferingPhysician_TextChanged);
            // 
            // linkDoc
            // 
            this.linkDoc.AutoSize = true;
            this.linkDoc.Font = new System.Drawing.Font("Bahnschrift", 8F);
            this.linkDoc.Location = new System.Drawing.Point(257, 235);
            this.linkDoc.Name = "linkDoc";
            this.linkDoc.Size = new System.Drawing.Size(79, 13);
            this.linkDoc.TabIndex = 16;
            this.linkDoc.TabStop = true;
            this.linkDoc.Text = "Link goes here";
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Location = new System.Drawing.Point(140, 230);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(100, 30);
            this.btnStartProcess.TabIndex = 17;
            this.btnStartProcess.Text = "Erstellen";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(555, 274);
            this.Controls.Add(this.btnStartProcess);
            this.Controls.Add(this.linkDoc);
            this.Controls.Add(this.labelRefferingPhysician);
            this.Controls.Add(this.btnRefferingPhysician);
            this.Controls.Add(this.txtBoxRefferingPhysician);
            this.Controls.Add(this.labelIssues);
            this.Controls.Add(this.btnIssues);
            this.Controls.Add(this.txtBoxIssues);
            this.Controls.Add(this.labelRemarks);
            this.Controls.Add(this.btnRemarks);
            this.Controls.Add(this.txtBoxRemarks);
            this.Controls.Add(this.labelExaminer);
            this.Controls.Add(this.btnExaminer);
            this.Controls.Add(this.txtBoxExaminer);
            this.Controls.Add(this.labelPersonData);
            this.Controls.Add(this.btnPersonData);
            this.Controls.Add(this.txtBoxPersonData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainWindow";
            this.Text = "HL7-MessageGenerator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtBoxPersonData;
        public System.Windows.Forms.Button btnPersonData;
        public System.Windows.Forms.Label labelPersonData;
        public System.Windows.Forms.OpenFileDialog openPersonData;
        public System.Windows.Forms.Label labelExaminer;
        public System.Windows.Forms.Button btnExaminer;
        public System.Windows.Forms.TextBox txtBoxExaminer;
        public System.Windows.Forms.Label labelRemarks;
        public System.Windows.Forms.Button btnRemarks;
        public System.Windows.Forms.TextBox txtBoxRemarks;
        public System.Windows.Forms.Label labelIssues;
        public System.Windows.Forms.Button btnIssues;
        public System.Windows.Forms.TextBox txtBoxIssues;
        public System.Windows.Forms.Label labelRefferingPhysician;
        public System.Windows.Forms.Button btnRefferingPhysician;
        public System.Windows.Forms.TextBox txtBoxRefferingPhysician;
        public System.Windows.Forms.LinkLabel linkDoc;
        public System.Windows.Forms.Button btnStartProcess;
    }
}

