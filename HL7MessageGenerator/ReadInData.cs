﻿using System;
using System.Text.RegularExpressions;

namespace HL7MessageGenerator
{
    public class ReadInData
    {
        public String[] pID = new String[10000];
        public String[] pLastName = new String[10000];
        public String[] pFirstName = new String[10000];
        public String[] pExtraName = new String[10000];
        public String[] pTitle = new String[10000];
        public String[] pbDay = new String[10000];
        public String[] pAddrStreet = new String[10000];
        public String[] pAddrCity = new String[10000];
        public String[] pAddrZIP = new String[10000];
        public String[] pAddrCountry = new String[10000];
        public String[] pSex = new String[10000];
        public String[] pPhoneHome = new String[10000];
        public String[] pPhoneBusiness = new String[10000];
        public String[] pProfession = new String[10000];
        public String[] pVisitNr = new String[10000];
        public String[] issues = new String[10000];
        public String[] remarks = new String[10000];
        public String[] refferingPhysician = new String[10000];
        public String[] exLName = new String[10000];
        public String[] exFName = new String[10000];
        public String[] exLogin = new String[10000];
        public String[] rpSName = new String[10000];
        public String[] rpLName = new String[10000];
        public String[] rpFName = new String[10000];
        public String[] rpEName = new String[10000];
        public String[] rpTitle = new String[10000];
        public String[] rpDepartment = new String[10000];
        public String[] rpFacility = new String[10000];
        public bool[] isCorrect = new bool[10000];

        public int counter = 0;

        public void ReadIn(String path, String type)
        {
            counter = 0;
            String line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                switch (type)
                {
                    case "Personendaten":
                        PersonData(line, counter);
                        break;
                    case "Untersucher":
                        Examiner(line, counter);
                        break;
                    case "Überweiser":
                        RefferingPhysician(line, counter);
                        break;
                    case "Fragestellungen":
                        Issues(line, counter);
                        break;
                    case "Anmerkungen":
                        Remarks(line, counter);
                        break;
                }
                counter++;
            }
        }
        //Split a line of PersonData to several Parts. And after that all parts will be assigned to their respective String Array
        public void PersonData(String line, int counter)
        {
            //Split the line after a tab
            String[] part = Regex.Split(line, "\t");

            //Check if ID, FirstName or LastName is Empty. If yes mark this one as not complete and go back. All other Fields are optional.
            if (part[0].Length == 0 || part[1].Length == 0 || part[3].Length == 0)
            {
                isCorrect[counter] = false;
                pVisitNr[counter] = "00000";
                return;
            }

            //ID: Bring it in the right format
            int id = int.Parse(part[0]);
            pID[counter] = "A"+id.ToString("D7");

            //VisitNr: Calculated  by CurrentTime in ms * ID / 100000000
            pVisitNr[counter] = (System.DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() * id / 100000000).ToString();

            //LastName
            pLastName[counter] = part[1];

            //ExtraName
            pExtraName[counter] = part[2];

            //FirstName
            pFirstName[counter] = part[3];

            //Title
            pTitle[counter] = part[4];

            //Birthday
            pbDay[counter] = part[5];

            //Adress
            pAddrStreet[counter] = part[6];
            pAddrCity[counter] = part[7];
            pAddrZIP[counter] = part[8];
            pAddrCountry[counter] = part[9];

            //Sex: Check if it is M or F. If it isnt either it will be U for Unknown.
            if (part[10].Equals("M") || part[10].Equals("F"))
            {
                pSex[counter] = part[10];
            }
            else
            {
                pSex[counter] = "U";
            }

            //Phone Business and Home
            pPhoneHome[counter] = part[11];
            pPhoneBusiness[counter] = part[12];

            //Profession
            pProfession[counter] = part[13];
        }

        //Split a line of Examiner to several Parts. And after that all parts will be assigned to their respective String Array
        public void Examiner(String line, int counter)
        {
            //Split a line after every tab.
            String[] part = Regex.Split(line, "\t");

            //LastName:
            exLName[counter] = part[0];

            //FirstName:
            exFName[counter] = part[1];

            //LoginName:
            exLogin[counter] = part[2];
        }

        //Assigns a line to the issues array.
        public void Issues(String line, int counter)
        {
            issues[counter] = line;
        }

        //Assigns a line to the remarks array.
        public void Remarks(String line, int counter)
        {
            remarks[counter] = line;
        }

        //Split a line of RefferingPhysician to several Parts. And after that all parts will be assigned to their respective String Array
        public void RefferingPhysician(String line, int counter)
        {
            //Split the line after each tab
            String[] part = Regex.Split(line, "\t");

            //LastName:
            rpLName[counter] = part[0];

            //FirstName:
            rpFName[counter] = part[1];

            //Facility:
            rpFacility[counter] = part[2];

            //Department:
            rpDepartment[counter] = part[3];

            //ExtraName:
            rpEName[counter] = part[4];

            //Title
            rpTitle[counter] = part[5];

            //ShortName
            rpSName[counter] = part[6];

        }
    }
}
