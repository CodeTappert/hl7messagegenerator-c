﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace HL7MessageGenerator
{
    class Methods
    {
        public static int firstExaminer = 0, secondExaminer = 0;
        static int a01 = 0, a03 = 0, a04 = 0, a08 = 0, orm = 0;
        public static Random random = new Random(), randomHour = new Random(), randomMinutes = new Random(1234), randomSeconds = new Random(4321);
        public static String examKindLong = "", path = "./Log.txt", log = "";

        //Opens a "Open File" Dialog and returns the Path of the choosen File
        public static String OpenFileDialog(String type)
        {
            String path = "";

            OpenFileDialog fileDialog = new OpenFileDialog();

            //Should be where the .exe File is
            fileDialog.InitialDirectory = "./";

            //Name of the Dialog. 
            fileDialog.Title = type + " öffnen";

            //Possible Extensions. Only .txt is allowed
            fileDialog.Filter = "txt Dateien (*.txt)|*.txt";

            //Limit Number of possible Extensions in the Extension List
            fileDialog.FilterIndex = 1;

            //Set Default Extension (may be unused)
            fileDialog.DefaultExt = "*.txt";

            //If a File is choosen
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                //get FilePath
                path = fileDialog.FileName;
            }


            return path;
        }

        internal static int GetRefferingPhysician()
        {
            return random.Next(PatientVisit.GetFileLength("Überweiser"));
        }

        //Returns an int which declares who is the second Examiner.
        public static int SecondExaminer(ReadInData readIn)
        {
            //Picks a Random Number between 0 and the Length of the Examiner File
            secondExaminer = random.Next(PatientVisit.GetFileLength("Untersucher"));

            //If the choosen Examiner isnt the same as the first
            if (secondExaminer != firstExaminer)
            {
                //return the integer
                return secondExaminer;
            }
            //If it is the same call the method again
            SecondExaminer(readIn);

            //Has to be here. Even if it makes no sense
            return 0;
        }

        //Returns an int which declares who is the first Examiner
        public static int FirstExaminer(ReadInData readIn)
        {
            //Picks a Random Number between 0 and the Length of the Examiner File
            firstExaminer = random.Next(PatientVisit.GetFileLength("Untersucher"));

            //Return the Integer
            return firstExaminer;
        }

        internal static string OrderID()
        {
            return (random.Next(89999) + 10000).ToString();
        }

        internal static string ChooseStation()
        {
            switch (random.Next(10) + 1)
            {
                case (1):
                    return "Station 1";
                case (2):
                    return "Station 2";
                case (3):
                    return "Station 3";
                case (4):
                    return "Station 4";
                case (5):
                    return "Station 5";
                case (6):
                    return "Station 6";
                case (7):
                    return "Station 7";
                case (8):
                    return "Station 8";
                case (9):
                    return "Station 9";
                case (10):
                    return "Station 10";
                default:
                    return "Station 11";
            }
        }

        public static string ExamKindShort()
        {
            switch (random.Next(6) + 1)
            {
                case (1):
                    examKindLong = "Sonographie";
                    return "SONO";
                case (2):
                    examKindLong = "Echographie";
                    return "ECHO";
                case (3):
                    examKindLong = "Kardiographie";
                    return "KARDIO";
                case (4):
                    examKindLong = "Stereographie";
                    return "STEREO";
                case (5):
                    examKindLong = "Endoskopie";
                    return "ENDO";
                case (6):
                    examKindLong = "Gynolographie";
                    return "GYNO";
                default:
                    return null;
            }
        }

        internal static string SubOrderID()
        {
            return OrderID();
        }

        internal static int GetIssues()
        {
            return random.Next(PatientVisit.GetFileLength("Fragestellungen"));
        }

        internal static int GetRemarks()
        {
            return random.Next(PatientVisit.GetFileLength("Anmerkungen"));
        }

        //Make the files. 
        public static void GenerateFile(ReadInData readIn, int count, string type, string content)
        {
            switch (type)
            {
                case ("A01"):
                    a01++;
                    break;
                case ("A03"):
                    a03++;
                    break;
                case ("A04"):
                    a04++;
                    break;
                case ("A08"):
                    a08++;
                    break;
                case ("ORM"):
                    orm++;
                    break;
                default:
                    break;
            }
            if (!Directory.Exists("./Files"))
            {
                Directory.CreateDirectory("./Files");
            }
            //Path is the current directory/files and the name is the id + type of message + the time
            String path = Directory.GetCurrentDirectory() + "/Files/" + type + " " + readIn.pID[count] + " " + DateTime.Now.ToString("HHmmssff") + ".hl7";
            Methods.WriteLog("Datei: " + path + " erstellt");


            //Write the file
            File.WriteAllText(path, content, Encoding.GetEncoding("windows-1250"));
        }

        internal static string Urgency()
        {
            switch (random.Next(6))
            {
                case (0):
                    return "Sehr Dringend";
                case (1):
                    return "Nicht Dringend";
                case (2):
                    return "Notfall";
                case (3):
                    return "Jetzt oder nie";
                case (4):
                    return "Irgendwann mal";
                case (5):
                    return "Ist hier jemand Arzt?";
                default:
                    return null;
            }
        }

        internal static string ExamKindLong()
        {
            return examKindLong;
        }

        //Method for getting the current time in milliseconds
        public static string GetCurrentTimeMillis()
        {
            return (DateTime.Now.Ticks / 10000).ToString();
        }

        public static void TextBoxSetText(TextBox textbox, String path)
        {
            textbox.Text = path;
        }

        public static long[] RandomDate()
        {
            long[] array = new long[4];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = GetDate();
            }

            Array.Sort(array);
            return array;
        }

        public static long GetDate()
        {
            DateTime date = DateTime.Today;
            int range = 20 * 365; //20 Years
            date = date.AddDays(-random.Next(range));
            date = date.AddHours(randomHour.Next(24));
            date = date.AddMinutes(randomMinutes.Next(60));
            date = date.AddSeconds(randomSeconds.Next(60));
            Console.WriteLine(date);
            return long.Parse(date.ToString("yyyyMMddhhmmss"));
        }

        public static string CurrentDateAndTime()
        {
            DateTime dateTime = DateTime.Now;
            return dateTime.ToString("yyyyMMddHHmmss");
        }

        public static string Emergency()
        {
            int randomNumber = random.Next(3);
            if (randomNumber == 0)
            {
                return "I";
            }
            if (randomNumber == 1)
            {
                return "E";
            }
            if (randomNumber == 2)
            {
                return "O";
            }
            else
            {
                return null;
            }
        }

        internal static string InsurancePrivat()
        {
            Random random = new Random(2);
            int randomNumber = random.Next();
            if (randomNumber == 1)
            {
                return "g";
            }
            else
            {
                return "p";
            }
        }
        public static void WriteLog(String text)
        {
            String line = Methods.GetLogDate() + " " + text + "\r";
            log += line;

        }

        public static void MakeLog()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.WriteAllText(path, log);
        }

        private static string GetLogDate()
        {
            return DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss:fff");
        }

        public static void GiveStatistics()
        {
            WriteLog("Es wurden: " + a01 + " A01-Nachrichten, " + a03 + " A03-Nachrichten, " + a04 + " A04-Nachrichten, " + a08 + " A08 Nachrichten und " + orm + " ORM Nachrichten erstellt.");
            a01 = 0;
            a03 = 0;
            a04 = 0;
            a08 = 0;
            orm = 0;
        }


    }
}
