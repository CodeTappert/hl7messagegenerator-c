﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace HL7MessageGenerator
{
    public partial class MainWindow : Form
    {
        public String pathPersonData, pathExaminer, pathRefferingPhysician, pathIssues, pathRemarks;
        private void txtBoxExaminer_TextChanged(object sender, EventArgs e)
        {
            pathExaminer = txtBoxExaminer.Text;
        }

        private void btnExaminer_Click(object sender, EventArgs e)
        {
            pathExaminer = Methods.OpenFileDialog("Untersucher");
            Methods.TextBoxSetText(txtBoxExaminer, pathExaminer);
        }

        private void txtBoxRefferingPhysician_TextChanged(object sender, EventArgs e)
        {
            pathRefferingPhysician = txtBoxRefferingPhysician.Text;
        }

        private void btnRefferingPhysician_Click(object sender, EventArgs e)
        {
            pathRefferingPhysician = Methods.OpenFileDialog("Überweiser");
            Methods.TextBoxSetText(txtBoxRefferingPhysician, pathRefferingPhysician);
        }
        private void txtBoxIssues_TextChanged(object sender, EventArgs e)
        {
            pathIssues = txtBoxIssues.Text;
        }

        private void btnIssues_Click(object sender, EventArgs e)
        {
            pathIssues = Methods.OpenFileDialog("Fragestellungen");
            Methods.TextBoxSetText(txtBoxIssues, pathIssues);
        }
        private void txtBoxRemarks_TextChanged(object sender, EventArgs e)
        {
            pathRemarks = txtBoxRemarks.Text;
        }

        private void btnRemarks_Click(object sender, EventArgs e)
        {
            pathRemarks = Methods.OpenFileDialog("Anmerkungen");
            Methods.TextBoxSetText(txtBoxRemarks, pathRemarks);
        }



        public MainWindow()
        {
            InitializeComponent();

        }

        private void txtBoxPersonData_TextChanged(object sender, EventArgs e)
        {
            pathPersonData = txtBoxPersonData.Text;
        }

        private void btnPersonData_Click(object sender, EventArgs e)
        {
            pathPersonData = Methods.OpenFileDialog("Personendaten");
            Methods.TextBoxSetText(txtBoxPersonData, pathPersonData);
        }

        private void btnStartProcess_Click(object sender, EventArgs e)
        {
            CheckPaths();
            PatientVisit.SetPaths(pathPersonData, pathExaminer, pathRefferingPhysician, pathRemarks, pathIssues);
            btnStartProcess.Text = "Vorgang läuft";
            btnStartProcess.Enabled = false;
            Thread visit = new Thread(new ThreadStart(Start));
            visit.Start();
            visit.Join();
            btnStartProcess.Text = "Erstellen";
            btnStartProcess.Enabled = true;
        }

        public static void Start()
        {
            PatientVisit.GenerateVisit();
        }

        public void CheckPaths()
        {
            if (pathPersonData is null)
            {
                pathPersonData = "./defaulttxt/PersonDataShort.txt";
            }
            if (pathExaminer is null)
            {
                pathExaminer = "./defaulttxt/Examiner.txt";
            }
            if (pathRefferingPhysician is null)
            {
                pathRefferingPhysician = "./defaulttxt/RefferingPhysician.txt";
            }
            if (pathRemarks is null)
            {
                pathRemarks = "./defaulttxt/Remarks.txt";
            }
            if (pathIssues is null)
            {
                pathIssues = "./defaulttxt/Issues.txt";
            }
        }



    }
}
