﻿using System;

namespace HL7MessageGenerator
{
    public class GenerateA03
    {

        String content = "";
        public void A03(int counter, ReadInData readIn)
        {
            CreateA03(counter, readIn);
            Methods.GenerateFile(readIn, counter, "A03", content);
            Methods.WriteLog("Für Patient " + counter + " wurde eine A08 Nachricht erstellt");

        }
        public void CreateA03(int counter, ReadInData readIn)
        {
            content = "MSH|^~\\&|SonoWin|SonoWin|SonoWin|SonoWin|" + Methods.CurrentDateAndTime() + "||ADT^A03|" + Methods.GetCurrentTimeMillis() + "|P|2.6\rPID|||" +
                    readIn.pID[counter] + "||" + readIn.pLastName[counter] + "^" + readIn.pFirstName[counter] + "^^" + readIn.pExtraName[counter] + "^^" + readIn.pTitle[counter] + "||" + readIn.pbDay[counter] + "|" + readIn.pSex[counter] + "|||" +
                    readIn.pAddrStreet[counter] + "^^" + readIn.pAddrCity[counter] + "^^" + readIn.pAddrZIP[counter] + "^" + readIn.pAddrCountry[counter] + "||" + readIn.pPhoneHome[counter] + "|" + readIn.pPhoneBusiness[counter] + "|||||||||||||" + readIn.pProfession[counter] +
                    "\rPV1||" + Methods.Emergency() + "|||||||||||||||||" + readIn.pVisitNr[counter] + "|||||||||||||||||||||||||" + PatientVisit.GetDate(0) + "|" + PatientVisit.GetDate(3) + "\rIN1|||||||||||||||" + Methods.InsurancePrivat();
        }

    }
}