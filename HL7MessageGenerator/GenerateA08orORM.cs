﻿using System;

namespace HL7MessageGenerator
{
    public class GenerateA08orORM
    {
        String content;
        Random random = new Random();
        int[] subjectID = new int[15]; //Order is the order in the ReadIn Method
        public void A08orORM(int patient, ReadInData readIn)
        {

            if (random.Next(2) != 1)
            {
                IsSubjectToChange(readIn, patient);
                GenerateA08(readIn, patient);
                Methods.GenerateFile(readIn, patient, "A08", content);
                Methods.WriteLog("Für Patient " + patient+ " wurde eine A08 Nachricht erstellt");

            }
            else
            {
                Methods.FirstExaminer(readIn);
                Methods.SecondExaminer(readIn);
                GenerateORM(readIn, patient);
                Methods.GenerateFile(readIn, patient, "ORM", content);
                Methods.WriteLog("Für Patient " + patient+ " wurde eine ORM Nachricht erstellt");

            }


            if (random.Next(4) != 1)
            {
                A08orORM(patient, readIn);
            }
        }

        private void GenerateORM(ReadInData readIn, int patient)
        {
            int refferingPhysician = Methods.GetRefferingPhysician();
            int firstExaminer = Methods.FirstExaminer(readIn), secondExaminer = Methods.SecondExaminer(readIn);

            content = "MSH|^&~\\|CLINICOM|PRD|SONOWIN|SONOWIN|" + Methods.CurrentDateAndTime() + "|203|ORM^O01|" + Methods.GetCurrentTimeMillis() + "|P|2.6||||AL\rPID|1||" + readIn.pID[patient] + "||" +
                readIn.pLastName[patient] + "^" + readIn.pFirstName[patient] + "^" + readIn.pExtraName[patient] + "^^^" + readIn.pTitle[patient] + "||"
                + readIn.pbDay[patient] + "|" + readIn.pSex[patient] + "|||" + readIn.pAddrStreet[patient] + "^^" + readIn.pAddrCity[patient] + "^^" + readIn.pAddrZIP[patient] + "^" +
                readIn.pAddrCountry[patient] + "||" + readIn.pPhoneHome[patient] + "|" + readIn.pPhoneBusiness[patient] + "||U|VAR||||||||||" + readIn.pProfession[patient] + "\rPV1|1|" + Methods.Emergency() +
                "|||||||||||||||||" + readIn.pVisitNr[patient] + "|||||||||||||||||||||||||200706191535\rORC|NW|" + Methods.OrderID() + "|" + Methods.ChooseStation() + "||||||" +
                PatientVisit.GetDate(1) + "|" + readIn.rpSName[refferingPhysician] + "^" + readIn.rpLName[refferingPhysician] + "^" + readIn.rpFName[refferingPhysician] + "^" + readIn.rpEName[refferingPhysician] + "^^^" +
                readIn.rpTitle[refferingPhysician] + "^^" + readIn.rpDepartment[refferingPhysician] + "^^^^^" + readIn.rpFacility[refferingPhysician] + "|||" + Methods.ChooseStation() + "|||||||\rOBR|1|" + Methods.OrderID() +
                "^" + Methods.SubOrderID() + "||" + Methods.ExamKindShort() + "^" + Methods.ExamKindLong() + "|" +
                Methods.Urgency() + "||||||||||||||||||||||" + PatientVisit.GetDate(2) + "||||" + readIn.issues[Methods.GetIssues()] + "^" + readIn.remarks[Methods.GetRemarks()] + "|" + readIn.exLogin[firstExaminer] + "\\" +
                readIn.exLName[firstExaminer] + "\\" + readIn.exFName[firstExaminer] + "|" + readIn.exLogin[secondExaminer] + "\\" + readIn.exLName[secondExaminer] + "\\" +
                readIn.exFName[secondExaminer] + "|||";
        }

        private void GenerateA08(ReadInData readIn, int patient)
        {
            content = "MSH|^~\\&|Sonowin|Sonowin|Sonowin|Sonowin|" + Methods.CurrentDateAndTime() + "||ADT^A08|" + Methods.GetCurrentTimeMillis() + "|P|2.6\rEVN|A08|"
                + Methods.CurrentDateAndTime() +"\rPID|||" + readIn.pID[patient] + "||" + readIn.pLastName[subjectID[1]] + "^" + readIn.pFirstName[subjectID[3]] + "^^" +
                readIn.pExtraName[subjectID[2]] + "^^" + readIn.pTitle[subjectID[4]] + "||" + readIn.pbDay[subjectID[5]] + "|" + readIn.pSex[subjectID[10]] + "|||" + readIn.pAddrStreet[subjectID[6]]
                + "^^" + readIn.pAddrCity[subjectID[7]] + "^^" + readIn.pAddrZIP[subjectID[8]] + "^^" + readIn.pAddrCountry[subjectID[9]] + "||" + readIn.pPhoneHome[subjectID[11]] + "|" + readIn.pPhoneBusiness[subjectID[12]] + "|||||||||||||"
                + readIn.pProfession[subjectID[13]] + "\rPV1||" + Methods.Emergency() + "\rIN1|||||||||||||||" + Methods.InsurancePrivat();
        }

        public void IsSubjectToChange(ReadInData readIn, int counter)
        {
            for (int i = 0; i < subjectID.Length; i++)
            {
                if (random.Next() != 1)
                {
                    subjectID[i] = ExchangePatient(readIn);
                }
                else
                {
                    subjectID[i] = counter;
                }
            }
        }

        public int ExchangePatient(ReadInData readIn)
        {
            Random randomPatient = new Random();
            int exchangePatient = randomPatient.Next(PatientVisit.GetFileLength("Personendaten") - 1);
            if (readIn.isCorrect[exchangePatient] == true)
            {
                ExchangePatient(readIn);
            }
            return exchangePatient;

        }
    }
}