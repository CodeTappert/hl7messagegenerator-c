﻿using System;
using System.IO;
using System.Linq;

namespace HL7MessageGenerator
{
    public class PatientVisit
    {
        public static String pathPersonData, pathExaminer, pathRefferingPhysician, pathRemarks, pathIssues;
        public static int[] fileLength = new int[5];
        public static int patient;
        public static long[] randomDate = new long[4]; //0=ADmissionDate,1=ExamOrdered,2=RequestDate,3=DischargeDate
        static ReadInData readin = new ReadInData();
        static GenerateA01orA04 generateA01OrA04 = new GenerateA01orA04();
        static GenerateA08orORM generateA08OrORM = new GenerateA08orORM();
        static GenerateA03 generateA03 = new GenerateA03();
        static Random random = new Random();

        public static void GenerateVisit()
        {
            SetFileLength();
            Methods.WriteLog("Länge der Dateien bestimmt.");
            ReadIn();
            Methods.WriteLog("Dateien eingelesen");

            for (patient = 0; patient < fileLength[0]; patient++)
            {
                randomDate = Methods.RandomDate();
                generateA01OrA04.A01orA04(patient, readin);

                if (random.Next(30) != 1)
                {
                    generateA08OrORM.A08orORM(patient, readin);
                }

                if (random.Next(2) != 1)
                {
                    generateA03.A03(patient, readin);
                }
            }

            Methods.WriteLog("Vorgang beendet.");
            Methods.GiveStatistics();
            Methods.MakeLog();

        }

        public static void SetFileLength()
        {
            fileLength[0] = File.ReadLines(pathPersonData).Count();
            fileLength[1] = File.ReadLines(pathExaminer).Count();
            fileLength[2] = File.ReadLines(pathRefferingPhysician).Count();
            fileLength[3] = File.ReadLines(pathRemarks).Count();
            fileLength[4] = File.ReadLines(pathIssues).Count();
        }

        public static int GetFileLength(String type)
        {
            switch (type)
            {
                case ("Personendaten"):
                    return fileLength[0];
                case ("Untersucher"):
                    return fileLength[1];
                case ("Überweiser"):
                    return fileLength[2];
                default:
                    return 0;

            }
        }

        public static void SetPaths(String pPersonData, String pExaminer, String pRefferingPhysician, String pRemarks, String pIssues)
        {
            pathPersonData = pPersonData;
            pathExaminer = pExaminer;
            pathRefferingPhysician = pRefferingPhysician;
            pathRemarks = pRemarks;
            pathIssues = pIssues;

        }

        public static void ReadIn()
        {
            readin.ReadIn(pathPersonData, "Personendaten");
            Methods.WriteLog("Personendaten wurden eingelesen.");
            readin.ReadIn(pathExaminer, "Untersucher");
            Methods.WriteLog("Untersucher wurden eingelesen.");
            readin.ReadIn(pathRefferingPhysician, "Überweiser");
            Methods.WriteLog("Überweiser wurden eingelesen.");
            readin.ReadIn(pathRemarks, "Anmerkungen");
            Methods.WriteLog("Anmerkungen eingelesen.");
            readin.ReadIn(pathIssues, "Fragestellungen");
            Methods.WriteLog("Fragestellungen eingelesen.");
        }

        public static String GetDate(int arrayDigit)
        {
            return randomDate[arrayDigit].ToString();
        }

        public static void DeleteDefaultTxt()
        {
            if (Directory.Exists("./defaulttxt"))
            {
                Directory.Delete("./defaulttxt", true);
            }
        }
    }
}
