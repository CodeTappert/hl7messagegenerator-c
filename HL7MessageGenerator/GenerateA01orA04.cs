﻿using System;

namespace HL7MessageGenerator
{
    class GenerateA01orA04
    {
        Random random = new Random(0);
        String content;
        public void A01orA04(int count, ReadInData readIn)
        {
            int a01ora04 = random.Next(2);
            Console.WriteLine(a01ora04);
            if (a01ora04 != 1)
            {
                Methods.WriteLog("Es ist nicht:" + a01ora04);

                CreateA01orA04("A01", readIn, count);
                Methods.GenerateFile(readIn, count, "A01", content);
                Methods.WriteLog("Für Patient " + count + " wurde eine A01 Nachricht erstellt");

            }
            else
            {
                Methods.WriteLog("Es ist:" + a01ora04);
                CreateA01orA04("A04", readIn, count);
                Methods.GenerateFile(readIn, count, "A04", content);
                Methods.WriteLog("Für Patient " + count + " wurde eine A04 Nachricht erstellt");

            }
        }

        public void CreateA01orA04(String type, ReadInData readIn, int count)
        {
             content = "MSH|^~\\&|SonoWin|SonoWin|SonoWin|SonoWin|" + Methods.CurrentDateAndTime() + "||ADT^" + type + "|" + Methods.GetCurrentTimeMillis()
            + "|P|2.6\rPID|||" +
                readIn.pID[count] + "||" + readIn.pLastName[count] + "^" + readIn.pFirstName[count] + "^^" + readIn.pExtraName[count] + "^^" + readIn.pTitle[count] + "||" + readIn.pbDay[count] + "|" + readIn.pSex[count] + "|||" +
                readIn.pAddrStreet[count] + "^^" + readIn.pAddrCity[count] + "^^" + readIn.pAddrZIP[count] + "^" + readIn.pAddrCountry[count] + "||" + readIn.pPhoneHome[count] + "|" + readIn.pPhoneBusiness[count] + "|||||||||||||" + readIn.pProfession[count] +
                "\rPV1||" + Methods.Emergency() + "|||||||||||||||||" + readIn.pVisitNr[count] + "|||||||||||||||||||||||||" + PatientVisit.GetDate(0) + "\rIN1|||||||||||||||" + Methods.InsurancePrivat();
        }
    }
}
