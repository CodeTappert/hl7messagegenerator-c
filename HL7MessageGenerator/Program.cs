﻿using System;
using System.Windows.Forms;

namespace HL7MessageGenerator
{
    static class StartProgramm
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
        static void OnProcessExit(object sender, EventArgs e)
        {
            PatientVisit.DeleteDefaultTxt();
        }
    }
}
